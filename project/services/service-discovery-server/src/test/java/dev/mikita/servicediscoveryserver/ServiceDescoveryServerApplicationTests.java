package dev.mikita.servicediscoveryserver;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * The type Service descovery server application tests.
 */
@SpringBootTest
class ServiceDescoveryServerApplicationTests {

    /**
     * Context loads.
     */
    @Test
	void contextLoads() {
	}

}
