package dev.mikita.notificationservice.controller;

import dev.mikita.notificationservice.entity.ChangeIssueStatusNotification;
import dev.mikita.notificationservice.service.NotificationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class NotificationControllerTest {

    @Mock
    private NotificationService notificationService;

    @InjectMocks
    private NotificationController notificationController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetNotification() {
        Long notificationId = 1L;
        ChangeIssueStatusNotification expectedNotification = new ChangeIssueStatusNotification();

        when(notificationService.findNotification(notificationId)).thenReturn(expectedNotification);

        ChangeIssueStatusNotification actualNotification = notificationController.getNotification(notificationId);

        verify(notificationService, times(1)).findNotification(notificationId);
        assertEquals(expectedNotification, actualNotification);
    }
}
