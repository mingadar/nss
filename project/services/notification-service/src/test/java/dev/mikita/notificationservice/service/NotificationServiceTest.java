package dev.mikita.notificationservice.service;

import dev.mikita.notificationservice.entity.ChangeIssueStatusNotification;
import dev.mikita.notificationservice.entity.IssueStatus;
import dev.mikita.notificationservice.exception.NotFoundException;
import dev.mikita.notificationservice.repository.ChangeIssueStatusNotificationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class NotificationServiceTest {
    @Mock
    private ChangeIssueStatusNotificationRepository notificationRepository;

    private NotificationService notificationService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        notificationService = new NotificationService(notificationRepository);
    }

    @Test
    public void testFindAllNotifications_WhenNotificationsExist() {
        String userId = "user123";
        List<ChangeIssueStatusNotification> notifications = new ArrayList<>();
        notifications.add(new ChangeIssueStatusNotification());
        when(notificationRepository.findAllByUserId(userId)).thenReturn(notifications);

        List<ChangeIssueStatusNotification> result = notificationService.findAllNotifications(userId);

        assertEquals(notifications, result);
        verify(notificationRepository, times(1)).findAllByUserId(userId);
    }

    @Test
    public void testFindAllNotifications_WhenNotificationsNotExist() {
        String userId = "user123";
        when(notificationRepository.findAllByUserId(userId)).thenReturn(new ArrayList<>());

        assertThrows(NotFoundException.class, () -> notificationService.findAllNotifications(userId));
        verify(notificationRepository, times(1)).findAllByUserId(userId);
    }

    @Test
    public void testFindNotification_WhenNotificationExists() {
        Long notificationId = 1L;
        ChangeIssueStatusNotification notification = new ChangeIssueStatusNotification();
        when(notificationRepository.findById(notificationId)).thenReturn(Optional.of(notification));

        ChangeIssueStatusNotification result = notificationService.findNotification(notificationId);

        assertEquals(notification, result);
        verify(notificationRepository, times(1)).findById(notificationId);
    }

    @Test
    public void testFindNotification_WhenNotificationDoesNotExist() {
        Long notificationId = 1L;
        when(notificationRepository.findById(notificationId)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> notificationService.findNotification(notificationId));
        verify(notificationRepository, times(1)).findById(notificationId);
    }

    @Test
    public void testCreateNotification_WithValidParameters() {
        Long issueId = 1L;
        String userId = "user123";
        IssueStatus issueStatus = IssueStatus.SOLVING;
        when(notificationRepository.save(any(ChangeIssueStatusNotification.class)))
                .thenAnswer(invocation -> invocation.getArgument(0));

        notificationService.createNotification(issueId, userId, issueStatus);

        verify(notificationRepository, times(1))
                .save(any(ChangeIssueStatusNotification.class));
    }

    @Test
    public void testCreateNotification_WithNullParameters() {
        assertThrows(IllegalArgumentException.class, () ->
                notificationService.createNotification(null, null, null));
        verify(notificationRepository, never()).save(any(ChangeIssueStatusNotification.class));
    }
}

