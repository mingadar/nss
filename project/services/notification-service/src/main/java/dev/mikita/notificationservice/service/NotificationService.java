package dev.mikita.notificationservice.service;

import dev.mikita.notificationservice.entity.ChangeIssueStatusNotification;
import dev.mikita.notificationservice.entity.IssueStatus;
import dev.mikita.notificationservice.exception.NotFoundException;
import dev.mikita.notificationservice.repository.ChangeIssueStatusNotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The type Notification service.
 */
@Service
public class NotificationService {
    private final ChangeIssueStatusNotificationRepository changeIssueStatusNotificationRepository;

    /**
     * Instantiates a new Notification service.
     *
     * @param changeIssueStatusNotificationRepository the change issue status notification repository
     */
    @Autowired
    public NotificationService(ChangeIssueStatusNotificationRepository changeIssueStatusNotificationRepository) {
        this.changeIssueStatusNotificationRepository = changeIssueStatusNotificationRepository;
    }

    /**
     * Find all notifications list.
     *
     * @param userId the user id
     * @return the list
     */
    @Transactional(readOnly = true)
    public List<ChangeIssueStatusNotification> findAllNotifications(String userId) {
        List<ChangeIssueStatusNotification> notifications = changeIssueStatusNotificationRepository.findAllByUserId(userId);
        if (notifications.isEmpty()) {
            throw new NotFoundException("Notifications are not found.");
        }
        return notifications;
    }

    /**
     * Find notification change issue status notification.
     *
     * @param id the id
     * @return the change issue status notification
     */
    @Transactional(readOnly = true)
    public ChangeIssueStatusNotification findNotification(Long id) {
        return changeIssueStatusNotificationRepository.findById(id).orElseThrow(
                () -> new NotFoundException("Notification is not found."));
    }

    /**
     * Create notification.
     *
     * @param issueId     the issue id
     * @param userId      the user id
     * @param issueStatus the issue status
     */
    @Transactional
    public void createNotification(Long issueId, String userId, IssueStatus issueStatus) {
        ChangeIssueStatusNotification changeIssueStatusNotification = new ChangeIssueStatusNotification();
        if (issueId == null || userId == null || issueStatus == null) {
            throw new IllegalArgumentException("Issue id, user id and issue status must be set.");
        }
        changeIssueStatusNotification.setIssueId(issueId);
        changeIssueStatusNotification.setUserId(userId);
        changeIssueStatusNotification.setIssueStatus(issueStatus);

        changeIssueStatusNotificationRepository.save(changeIssueStatusNotification);
    }
}
