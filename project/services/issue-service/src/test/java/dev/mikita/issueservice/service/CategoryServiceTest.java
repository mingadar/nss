package dev.mikita.issueservice.service;

import dev.mikita.issueservice.entity.Category;
import dev.mikita.issueservice.exception.NotFoundException;
import dev.mikita.issueservice.repository.CategoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class CategoryServiceTest {

    @Mock
    private CategoryRepository categoryRepository;

    @InjectMocks
    private CategoryService categoryService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetCategories() {
        List<Category> expectedCategories = new ArrayList<>();
        expectedCategories.add(new Category());
        expectedCategories.add(new Category());

        when(categoryRepository.findAll()).thenReturn(expectedCategories);
        List<Category> actualCategories = categoryService.getCategories();
        assertEquals(expectedCategories, actualCategories);
        verify(categoryRepository, times(1)).findAll();
    }

    @Test
    public void testGetCategoryById_ExistingId() {
        Long categoryId = 1L;
        Category expectedCategory = new Category();

        when(categoryRepository.findById(categoryId)).thenReturn(Optional.of(expectedCategory));

        Category actualCategory = categoryService.getCategoryById(categoryId);
        assertEquals(expectedCategory, actualCategory);
        verify(categoryRepository, times(1)).findById(categoryId);
    }

    @Test
    public void testGetCategoryById_NonExistingId() {
        Long categoryId = 1L;

        when(categoryRepository.findById(categoryId)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () ->
                categoryService.getCategoryById(categoryId));
        verify(categoryRepository, times(1)).findById(categoryId);
    }

    @Test
    public void testCreateCategory() {
        Category category = new Category();
        categoryService.createCategory(category);
        verify(categoryRepository, times(1)).save(category);
    }

    @Test
    public void testUpdateCategory_ExistingId() {
        Long categoryId = 1L;
        String newName = "Updated Category";
        Category existingCategory = new Category();

        when(categoryRepository.findById(categoryId)).thenReturn(Optional.of(existingCategory));

        categoryService.updateCategory(categoryId, newName);

        verify(categoryRepository, times(1)).findById(categoryId);
        assertEquals(newName, existingCategory.getName());
        verify(categoryRepository, times(1)).save(existingCategory);
    }

    @Test
    public void testUpdateCategory_NonExistingId() {
        Long categoryId = 1L;
        String newName = "Updated Category";

        when(categoryRepository.findById(categoryId)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () ->
                categoryService.updateCategory(categoryId, newName));
        verify(categoryRepository, times(1)).findById(categoryId);
    }

    @Test
    public void testDeleteCategory_ExistingId() {
        Long categoryId = 1L;
        when(categoryRepository.existsById(categoryId)).thenReturn(true);
        categoryService.deleteCategory(categoryId);

        verify(categoryRepository, times(1)).existsById(categoryId);
        verify(categoryRepository, times(1)).deleteById(categoryId);
    }

    @Test
    public void testDeleteCategory_NonExistingId() {
        Long categoryId = 1L;
        when(categoryRepository.existsById(categoryId)).thenReturn(false);
        assertThrows(NotFoundException.class, () ->
                categoryService.deleteCategory(categoryId));
        verify(categoryRepository, times(1)).existsById(categoryId);
    }
}

