package dev.mikita.issueservice.service;

import dev.mikita.issueservice.dto.ChangeIssueStatusNotificationDto;
import dev.mikita.issueservice.entity.Issue;
import dev.mikita.issueservice.entity.IssueReservation;
import dev.mikita.issueservice.entity.IssueStatus;
import dev.mikita.issueservice.exception.NotFoundException;
import dev.mikita.issueservice.repository.IssueRepository;
import dev.mikita.issueservice.repository.IssueReservationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class IssueReservationServiceTest {
    @Mock
    private IssueReservationRepository issueReservationRepository;

    @Mock
    private IssueRepository issueRepository;

    @Mock
    private KafkaTemplate<String, ChangeIssueStatusNotificationDto> kafkaTemplate;

    private IssueReservationService issueReservationService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        issueReservationService = new IssueReservationService(
                issueReservationRepository,
                issueRepository,
                kafkaTemplate
        );
    }

    @Test
    public void testCreateIssueReservation() {
        Long issueId = 1L;
        String serviceId = "service1";
        Issue issue = new Issue();
        issue.setStatus(IssueStatus.PUBLISHED);
        when(issueRepository.findById(issueId)).thenReturn(Optional.of(issue));

        issueReservationService.createIssueReservation(issueId, serviceId);

        verify(issueRepository, times(1)).findById(issueId);
        verify(issueReservationRepository, times(1)).save(any(IssueReservation.class));
        verify(kafkaTemplate, times(1)).send(anyString(), any(ChangeIssueStatusNotificationDto.class));
        assertEquals(IssueStatus.SOLVING, issue.getStatus());
    }

    @Test
    public void testCreateIssueReservation_issueNotFound() {
        Long issueId = 1L;
        String serviceId = "service1";
        when(issueRepository.findById(issueId)).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class,
                () -> issueReservationService.createIssueReservation(issueId, serviceId));
        verify(issueRepository, times(1)).findById(issueId);
        verify(issueReservationRepository, never()).save(any(IssueReservation.class));
        verify(kafkaTemplate, never()).send(anyString(), any(ChangeIssueStatusNotificationDto.class));
    }

    @Test
    public void testCreateIssueReservation_issueNotPublished() {
        Long issueId = 1L;
        String serviceId = "service1";
        Issue issue = new Issue();
        issue.setStatus(IssueStatus.SOLVED);
        when(issueRepository.findById(issueId)).thenReturn(Optional.of(issue));
        assertThrows(IllegalStateException.class,
                () -> issueReservationService.createIssueReservation(issueId, serviceId));
        verify(issueRepository, times(1)).findById(issueId);
        verify(issueReservationRepository, never()).save(any(IssueReservation.class));
        verify(kafkaTemplate, never()).send(anyString(), any(ChangeIssueStatusNotificationDto.class));
    }

    @Test
    public void testGetIssueReservation() {
        Long reservationId = 1L;
        IssueReservation reservation = new IssueReservation();
        when(issueReservationRepository.findById(reservationId)).thenReturn(Optional.of(reservation));

        IssueReservation result = issueReservationService.getIssueReservation(reservationId);
        assertEquals(reservation, result);

        verify(issueReservationRepository, times(1)).findById(reservationId);
    }

    @Test
    public void testGetIssueReservation_reservationNotFound() {
        Long reservationId = 1L;
        when(issueReservationRepository.findById(reservationId)).thenReturn(Optional.empty());
        assertThrows(NotFoundException.class,
                () -> issueReservationService.getIssueReservation(reservationId));
        verify(issueReservationRepository, times(1)).findById(reservationId);
    }

    @Test
    public void testGetIssuesReservationsCount() {
        String serviceId = "service1";
        long count = 5L;
        when(issueReservationRepository.countByServiceId(serviceId)).thenReturn(count);

        Long result = issueReservationService.getIssuesReservationsCount(serviceId);
        assertEquals(count, result);

        verify(issueReservationRepository, times(1)).countByServiceId(serviceId);
    }
}
