package dev.mikita.issueservice.controller;

import dev.mikita.issueservice.dto.request.CreateCategoryRequestDto;
import dev.mikita.issueservice.dto.request.UpdateCategoryRequestDto;
import dev.mikita.issueservice.dto.response.category.CategoryResponseDto;
import dev.mikita.issueservice.entity.Category;
import dev.mikita.issueservice.service.CategoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CategoryControllerTest {
    private CategoryController categoryController;

    @Mock
    private CategoryService categoryService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        categoryController = new CategoryController(categoryService);
    }

    @Test
    void testGetCategories() {
        List<Category> categories = Arrays.asList(
                new Category(),
                new Category()
        );
        when(categoryService.getCategories()).thenReturn(categories);
        ResponseEntity<List<CategoryResponseDto>> responseEntity = categoryController.getCategories();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(categories.size(), Objects.requireNonNull(responseEntity.getBody()).size());
        verify(categoryService, times(1)).getCategories();
    }

    @Test
    void testGetCategoryById() {
        Long categoryId = 1L;
        Category category = new Category();
        category.setId(categoryId);
        when(categoryService.getCategoryById(categoryId)).thenReturn(category);

        ResponseEntity<CategoryResponseDto> responseEntity = categoryController.getCategoryById(categoryId);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(category.getId(), Objects.requireNonNull(responseEntity.getBody()).getId());
        assertEquals(category.getName(), responseEntity.getBody().getName());
        verify(categoryService, times(1)).getCategoryById(categoryId);
    }

    @Test
    void testCreateCategory() {
        CreateCategoryRequestDto requestDto = new CreateCategoryRequestDto();
        requestDto.setName("New Category");
        categoryController.createCategory(requestDto);
        verify(categoryService, times(1)).createCategory(any(Category.class));
    }

    @Test
    void testUpdateCategory() {
        Long categoryId = 1L;
        String updatedName = "Updated Category";
        UpdateCategoryRequestDto requestDto = new UpdateCategoryRequestDto();
        requestDto.setName(updatedName);
        categoryController.updateCategory(categoryId, requestDto);
        verify(categoryService, times(1)).updateCategory(categoryId, updatedName);
    }

    @Test
    void testDeleteCategory() {
        Long categoryId = 1L;
        categoryController.deleteCategory(categoryId);
        verify(categoryService, times(1)).deleteCategory(categoryId);
    }
}

