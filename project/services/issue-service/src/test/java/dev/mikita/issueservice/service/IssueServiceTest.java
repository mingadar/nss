package dev.mikita.issueservice.service;

import dev.mikita.issueservice.dto.ChangeIssueStatusNotificationDto;
import dev.mikita.issueservice.entity.Issue;
import dev.mikita.issueservice.entity.IssueStatus;
import dev.mikita.issueservice.repository.IssueRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class IssueServiceTest {

    @Mock
    private IssueRepository issueRepository;

    @Mock
    private KafkaTemplate<String, ChangeIssueStatusNotificationDto> kafkaTemplate;

    @InjectMocks
    private IssueService issueService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testFindIssues() {
        Map<String, Object> filters = new HashMap<>();
        filters.put("status", IssueStatus.PUBLISHED);
        filters.put("authorId", "user123");
        filters.put("coordinates", null);
        filters.put("distanceM", 100.0);

        when(issueRepository.findAll(IssueStatus.PUBLISHED, "user123", null, 100.0)).thenReturn(Collections.emptyList());

        List<Issue> issues = issueService.findIssues(filters);
        assertNotNull(issues);
        assertEquals(0, issues.size());

        verify(issueRepository, times(1)).findAll(IssueStatus.PUBLISHED,
                "user123", null, 100.0);
    }

    @Test
    public void testFindIssueById() {
        Long issueId = 1L;
        Issue issue = new Issue();
        issue.setId(issueId);
        when(issueRepository.findById(issueId)).thenReturn(java.util.Optional.of(issue));

        Issue foundIssue = issueService.findIssueById(issueId);
        assertNotNull(foundIssue);
        assertEquals(issueId, foundIssue.getId());

        verify(issueRepository, times(1)).findById(issueId);
    }

    @Test
    public void testUpdateIssueStatus() {
        Long issueId = 1L;
        Issue issue = new Issue();
        issue.setId(issueId);
        when(issueRepository.findById(issueId)).thenReturn(java.util.Optional.of(issue));

        issueService.updateIssueStatus(issueId, IssueStatus.SOLVED);

        verify(issueRepository, times(1)).findById(issueId);

        assertEquals(IssueStatus.SOLVED, issue.getStatus());

        verify(kafkaTemplate, times(1)).send(eq("notifications"), any(ChangeIssueStatusNotificationDto.class));
    }
}
