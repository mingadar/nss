package dev.mikita.issueservice.controller;

import dev.mikita.issueservice.dto.response.issue.IssueResponseDto;
import dev.mikita.issueservice.entity.Issue;
import dev.mikita.issueservice.entity.IssueStatus;
import dev.mikita.issueservice.service.IssueService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public class IssueControllerTest {

    @Mock
    private IssueService issueService;


    @InjectMocks
    private IssueController issueController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetIssues() {
        List<Issue> issues = Arrays.asList(new Issue(), new Issue());
        when(issueService.findIssues(any())).thenReturn(issues);

        ResponseEntity<List<IssueResponseDto>> response = issueController.getIssues(null, null, null, null, null);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(issues.size(), Objects.requireNonNull(response.getBody()).size());
    }

    @Test
    public void testGetIssueById() {
        Issue issue = new Issue();
        issue.setId(1L);
        when(issueService.findIssueById(1L)).thenReturn(issue);

        ResponseEntity<IssueResponseDto> response = issueController.getIssueById(1L);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(issue.getId(), Objects.requireNonNull(response.getBody()).getId());
    }

    @Test
    public void testUpdateIssueStatus() {
        issueController.updateIssueStatus(1L, IssueStatus.SOLVED);
        verify(issueService).updateIssueStatus(1L, IssueStatus.SOLVED);
    }
}
