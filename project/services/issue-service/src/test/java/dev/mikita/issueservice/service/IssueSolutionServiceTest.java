package dev.mikita.issueservice.service;

import dev.mikita.issueservice.entity.Issue;
import dev.mikita.issueservice.entity.IssueReservation;
import dev.mikita.issueservice.entity.IssueSolution;
import dev.mikita.issueservice.repository.IssueRepository;
import dev.mikita.issueservice.repository.IssueReservationRepository;
import dev.mikita.issueservice.repository.IssueSolutionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class IssueSolutionServiceTest {
    @Mock
    private IssueSolutionRepository issueSolutionRepository;
    @Mock
    private IssueReservationRepository issueReservationRepository;
    @Mock
    private IssueRepository issueRepository;

    @InjectMocks
    private IssueSolutionService issueSolutionService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetIssueSolutionById() {
        Long id = 1L;
        IssueSolution issueSolution = new IssueSolution();
        when(issueSolutionRepository.findById(id)).thenReturn(Optional.of(issueSolution));

        IssueSolution result = issueSolutionService.getIssueSolutionById(id);
        assertEquals(issueSolution, result);
    }

    @Test
    void testCreateIssueSolutionWithoutReservation() {
        Long issueId = 1L;
        String serviceId = "serviceId";
        String description = "Description";
        MultipartFile photoFile = mock(MultipartFile.class);
        Issue issue = new Issue();
        issue.setId(issueId);
        when(issueRepository.findById(issueId)).thenReturn(Optional.of(issue));
        when(issueReservationRepository.getIssueReservationByIssueId(issueId)).thenReturn(null);

        assertThrows(IllegalStateException.class, () ->
                issueSolutionService.createIssueSolution(issueId, serviceId, description, photoFile));
    }

    @Test
    void testCreateIssueSolutionWithDifferentServiceId() {
        Long issueId = 1L;
        String serviceId = "serviceId";
        String description = "Description";
        MultipartFile photoFile = mock(MultipartFile.class);
        Issue issue = new Issue();
        issue.setId(issueId);
        IssueReservation issueReservation = new IssueReservation();
        issueReservation.setServiceId("otherServiceId");
        when(issueRepository.findById(issueId)).thenReturn(Optional.of(issue));
        when(issueReservationRepository.getIssueReservationByIssueId(issueId)).thenReturn(issueReservation);

        assertThrows(IllegalStateException.class, () ->
                issueSolutionService.createIssueSolution(issueId, serviceId, description, photoFile));
    }

    @Test
    void testGetIssuesSolutionsCount() {
        String serviceId = "serviceId";
        Long expectedCount = 10L;
        when(issueSolutionRepository.countByServiceId(serviceId)).thenReturn(expectedCount);

        Long result = issueSolutionService.getIssuesSolutionsCount(serviceId);
        assertEquals(expectedCount, result);
    }
}
