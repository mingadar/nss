package dev.mikita.issueservice.dto.response.likes;

import lombok.Data;

/**
 * The type Issue likes response dto.
 */
@Data
public class IssueLikesResponseDto {
    /**
     * The Count.
     */
    Long count;
}
