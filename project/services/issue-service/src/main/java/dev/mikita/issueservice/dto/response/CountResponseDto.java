package dev.mikita.issueservice.dto.response;

import lombok.Data;

/**
 * The type Count response dto.
 */
@Data
public class CountResponseDto {
    /**
     * The Count.
     */
    Long count;
}