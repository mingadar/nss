package dev.mikita.issueservice.dto.response.category;

import lombok.Data;

/**
 * The type Category response dto.
 */
@Data
public class CategoryResponseDto {
    /**
     * The Id.
     */
    Long id;
    /**
     * The Name.
     */
    String name;
}
