package dev.mikita.cacheservice;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import dev.mikita.issueservice.entity.Issue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CacheClient {

    private final IMap<Long, Issue> issueCache;

    @Autowired
    public CacheClient() {
        HazelcastInstance client = HazelcastClient.newHazelcastClient();
        issueCache = hazelcastInstance.getMap("issueCache");
    }

    public void addIssue(Issue issue) {
        issueCache.putIfAbsent(issue.getId(), issue);
    }

    public Issue getIssue(Long id) {
        return issueCache.get(id);
    }

    public void updateIssue(Issue issue) {
        issueCache.replace(issue.getId(), issue);
    }
}
