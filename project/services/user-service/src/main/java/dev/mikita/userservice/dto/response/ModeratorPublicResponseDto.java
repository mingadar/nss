package dev.mikita.userservice.dto.response;

import lombok.Data;

/**
 * The type Moderator public response dto.
 */
@Data
public class ModeratorPublicResponseDto {
    /**
     * The Uid.
     */
    String uid;
}
