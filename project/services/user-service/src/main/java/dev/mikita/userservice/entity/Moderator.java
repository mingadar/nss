package dev.mikita.userservice.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * The type Moderator.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Moderator extends User {
}
